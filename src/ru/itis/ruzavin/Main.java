package ru.itis.ruzavin;

import com.oracle.jrockit.jfr.InvalidValueException;

public class Main {
	public static void main(String[] args) {
		NotationConverter notationConverter = new NotationConverter();
		try {
			System.out.println(notationConverter.toArabic("dV"));
		} catch (InvalidValueException e) {
			throw new IllegalArgumentException(e);
		}
	}
}
