package ru.itis.ruzavin;

import com.oracle.jrockit.jfr.InvalidValueException;
import com.sun.javaws.exceptions.InvalidArgumentException;
import javafx.util.Pair;

import java.util.ArrayList;
import java.util.List;

public class NotationConverter {

	public int toArabic(String romanNotation) throws InvalidValueException {
		if (!checkString(romanNotation)) {
			throw new InvalidValueException(
					new IllegalArgumentException("String must contain only valid roman numerals [I, V, X, L, C, D, M]."));
		}
		char[] chars = romanNotation.toCharArray();
		if(romanNotation.length() == 1){
			return listOfAllRomanNumbers.get(findIndex(chars[0])).getValue();
		}
		int sum = 0;
		for (int i = 0; i < chars.length; i++) {
			int indexOfFirst = findIndex(chars[i]);
			int indexOfSecond = 7;
			if(i > 0) {
				indexOfSecond = findIndex(chars[i - 1]);
			}
			if (indexOfFirst > indexOfSecond) {
				sum += listOfAllRomanNumbers.get(indexOfFirst).getValue()
						- 2 * listOfAllRomanNumbers.get(indexOfSecond).getValue();
			} else {
				sum += listOfAllRomanNumbers.get(indexOfFirst).getValue();
			}
		}
		return sum;
	}

	private static final List<Pair<Character, Integer>> listOfAllRomanNumbers = new ArrayList<>();

	static {
		listOfAllRomanNumbers.add(new Pair<>('I', 1));
		listOfAllRomanNumbers.add(new Pair<>('V', 5));
		listOfAllRomanNumbers.add(new Pair<>('X', 10));
		listOfAllRomanNumbers.add(new Pair<>('L', 50));
		listOfAllRomanNumbers.add(new Pair<>('C', 100));
		listOfAllRomanNumbers.add(new Pair<>('D', 500));
		listOfAllRomanNumbers.add(new Pair<>('M', 1000));
	}

	private boolean checkString(String romanNotation) {
		return romanNotation.matches("^[IVXLCDM]+$") && romanNotation.length() != 0;
	}

	private int findIndex(Character character) {
		for (int i = 0; i < listOfAllRomanNumbers.size(); i++) {
			if (listOfAllRomanNumbers.get(i).getKey().equals(character)) {
				return i;
			}
		}
		return 0;
	}
}
